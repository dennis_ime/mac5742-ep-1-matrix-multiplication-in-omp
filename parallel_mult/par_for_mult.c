#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <assert.h>
#include <omp.h>

int main(int argc, char **argv)
{
   
  long **a, **b, **c;
  int N = 100, requested_threads;  
  double start_time, end_time;

  if (argc >= 2)
  {
    N = atoi (argv[1]);
    assert (N > 0);

    // Set up number of threads by console arguments if it has the third argument
    if(argc >= 3)
    {
      requested_threads = atoi(argv[2]);
      assert(requested_threads > 0);
      omp_set_num_threads(requested_threads);
    }
  }

  int i,j,k,mul=5;
  long col_sum = N * (N-1) / 2;

  a = (long **)malloc (N * sizeof(long *));
  b = (long **)malloc (N * sizeof(long *));
  c = (long **)malloc (N * sizeof(long *));

  for (i=0; i<N; i++) 
  {
    a[i] = (long *)malloc (N * sizeof(long));
    b[i] = (long *)malloc (N * sizeof(long));
    c[i] = (long *)malloc (N * sizeof(long));
  }


  for (i=0; i<N; i++)
  {
    for (j=0; j<N; j++) 
    {
      a[i][j] = i*mul;
      b[i][j] = i;
      c[i][j] = 0;
    }
  }

  printf ("Matrix generation finished.\n");         

  start_time = omp_get_wtime();
  
  #pragma omp for private(i, j, k)
  for (i=0; i<N; i++)
  {
    for (j=0; j<N; j++)
    {
      for (k=0; k<N; k++)
        c[i][j] += a[i][k] * b[k][j];
    }
  }
  end_time = omp_get_wtime();

  printf ("Multiplication finished.\n");         
  
  double time_elapsed = end_time - start_time;
  printf ("time elapsed: %f s\n", time_elapsed);

  for (i=0; i<N; i++)
  {
    for (j=0; j<N; j++)
      assert (c[i][j] == i*mul * col_sum);  
  }

  printf ("Test finished.\n");         

  return 0;
}