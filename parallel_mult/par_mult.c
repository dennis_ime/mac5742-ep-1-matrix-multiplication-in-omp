#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <assert.h>
#include <omp.h>

int main(int argc, char **argv)
{
   
  long **a, **b, **c;
  int N = 100, requested_threads;  
  double start_time, end_time;


  if (argc >= 2)
  {
    N = atoi (argv[1]);
    assert (N > 0);

    // Set up number of threads by console arguments if it has the third argument
    if(argc >= 3)
    {
      requested_threads = atoi(argv[2]);
      assert(requested_threads > 0);
      omp_set_num_threads(requested_threads);
    }
  }

  int i,j,k,mul=5;
  long col_sum = N * (N-1) / 2;

  a = (long **)malloc (N * sizeof(long *));
  b = (long **)malloc (N * sizeof(long *));
  c = (long **)malloc (N * sizeof(long *));

  for (i=0; i<N; i++) 
  {
    a[i] = (long *)malloc (N * sizeof(long));
    b[i] = (long *)malloc (N * sizeof(long));
    c[i] = (long *)malloc (N * sizeof(long));
  }


  for (i=0; i<N; i++)
  {
    for (j=0; j<N; j++) 
    {
      a[i][j] = i*mul;
      b[i][j] = i;
      c[i][j] = 0;
    }
  }

  printf ("Matrix generation finished.\n");         

  
  start_time = omp_get_wtime();  
  int number_of_threads;
  #pragma omp parallel 
  {
    int id, private_i, private_j, private_k; //private variables for the threads
    id = omp_get_thread_num();
    
    #pragma omp single
      number_of_threads = omp_get_num_threads();

    for (private_i = id; private_i < N; private_i += number_of_threads)
    {
      for (private_j = 0; private_j < N; private_j++)
      {
        for (private_k = 0; private_k < N; private_k++)
          c[private_i][private_j] += a[private_i][private_k] * b[private_k][private_j];        
      }
    }
  }

  end_time = omp_get_wtime();

  printf ("Multiplication finished.\n");         
  
  double time_elapsed = end_time - start_time;
    printf ("time elapsed: %f s\n", time_elapsed);

  for (i=0; i<N; i++)
  {
    for (j=0; j<N; j++)
      assert ( c[i][j] == i*mul * col_sum);  
  }

  printf ("Test finished.\n");         

  return 0;
}