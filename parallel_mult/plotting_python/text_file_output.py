from PromptCommand import *
from optparse import OptionParser
import collections

parser = OptionParser()
parser.add_option('-f', '--filename', dest='filename', help='output filename', metavar='FILE', default='report.txt')
parser.add_option('-s', '--matrixsize', dest='matrixsize', help='matrix dimension (square matrix)', default='100')
parser.add_option('-r', '--repetition', dest='repetition', help='number of repetition that mult programs will execute', default='10')

(options, args) = parser.parse_args()

filename = options.filename
matrix_size = int(options.matrixsize)
times = int(options.repetition)
		

commands = [SequentialMultCommand('../seq_mult.o', matrix_size),
			ParallelMultCommand('../par_for_mult.o', matrix_size, 1),
			ParallelMultCommand('../par_for_mult.o', matrix_size, 2),
			ParallelMultCommand('../par_for_mult.o', matrix_size, 3),
			ParallelMultCommand('../par_for_mult.o', matrix_size, 4),
			ParallelMultCommand('../par_for_mult.o', matrix_size, 8),
			ParallelMultCommand('../par_for_mult.o', matrix_size, 16)]


my_file = file(filename, 'w')
sum_for_each_entry = {}
key_order = []

for i in range(0, times):
	my_file.write('Execution #%d:\n'%i)
	for command in commands:
		(label, value) = command.execute()

		if not label in sum_for_each_entry:
			sum_for_each_entry[label] = 0.0
			key_order.append(label)
		else:
			sum_for_each_entry[label] += value

		my_file.write(label + ': ' + str(value) + '\n')

my_file.write('\nMean:\n')
for key in key_order:
	my_file.write(key + ': ' + str(sum_for_each_entry[key] / float(times)) + '\n')

my_file.close()
