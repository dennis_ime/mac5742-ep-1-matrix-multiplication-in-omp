Execution #0:
sequential: 0.012453
1 thread: 0.014594
2 threads: 0.01723
3 threads: 0.007723
4 threads: 0.009463
8 threads: 0.009371
16 threads: 0.010327
Execution #1:
sequential: 0.016496
1 thread: 0.022478
2 threads: 0.017675
3 threads: 0.007727
4 threads: 0.009502
8 threads: 0.008481
16 threads: 0.008926
Execution #2:
sequential: 0.016365
1 thread: 0.028126
2 threads: 0.013053
3 threads: 0.007759
4 threads: 0.009421
8 threads: 0.007415
16 threads: 0.008585
Execution #3:
sequential: 0.012891
1 thread: 0.025256
2 threads: 0.012303
3 threads: 0.007851
4 threads: 0.009739
8 threads: 0.007802
16 threads: 0.009572
Execution #4:
sequential: 0.016648
1 thread: 0.026075
2 threads: 0.018589
3 threads: 0.007637
4 threads: 0.009535
8 threads: 0.008044
16 threads: 0.009677
Execution #5:
sequential: 0.016278
1 thread: 0.028116
2 threads: 0.013034
3 threads: 0.007738
4 threads: 0.009532
8 threads: 0.008398
16 threads: 0.008979
Execution #6:
sequential: 0.016485
1 thread: 0.020907
2 threads: 0.020033
3 threads: 0.014733
4 threads: 0.014765
8 threads: 0.017731
16 threads: 0.007901
Execution #7:
sequential: 0.016613
1 thread: 0.011173
2 threads: 0.021299
3 threads: 0.014494
4 threads: 0.009477
8 threads: 0.011993
16 threads: 0.007579
Execution #8:
sequential: 0.016627
1 thread: 0.011175
2 threads: 0.02109
3 threads: 0.014554
4 threads: 0.009566
8 threads: 0.009303
16 threads: 0.010168
Execution #9:
sequential: 0.016502
1 thread: 0.026398
2 threads: 0.018636
3 threads: 0.007843
4 threads: 0.009787
8 threads: 0.008351
16 threads: 0.012121

Mean:
sequential: 0.0144905
1 thread: 0.0199704
2 threads: 0.0155712
3 threads: 0.0090336
4 threads: 0.0091324
8 threads: 0.0087518
16 threads: 0.0083508
