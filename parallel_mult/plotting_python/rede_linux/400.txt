Execution #0:
sequential: 1.117125
1 thread: 1.199051
2 threads: 0.836212
3 threads: 0.662963
4 threads: 0.564062
8 threads: 0.550979
16 threads: 0.536232
Execution #1:
sequential: 1.082722
1 thread: 1.186743
2 threads: 0.828132
3 threads: 0.85668
4 threads: 0.886723
8 threads: 0.580453
16 threads: 0.602026
Execution #2:
sequential: 1.068104
1 thread: 1.114472
2 threads: 0.593167
3 threads: 0.577337
4 threads: 0.567148
8 threads: 0.534111
16 threads: 0.527081
Execution #3:
sequential: 1.057153
1 thread: 1.168435
2 threads: 0.581906
3 threads: 0.602623
4 threads: 0.571595
8 threads: 0.534157
16 threads: 0.525098
Execution #4:
sequential: 1.065151
1 thread: 1.144027
2 threads: 0.595001
3 threads: 0.579158
4 threads: 0.574932
8 threads: 0.529901
16 threads: 0.515223
Execution #5:
sequential: 1.047641
1 thread: 1.136127
2 threads: 0.583558
3 threads: 0.577178
4 threads: 0.572787
8 threads: 0.543867
16 threads: 0.52991
Execution #6:
sequential: 1.055804
1 thread: 1.163187
2 threads: 0.658591
3 threads: 0.585918
4 threads: 0.578127
8 threads: 0.536447
16 threads: 0.575417
Execution #7:
sequential: 1.148502
1 thread: 1.200974
2 threads: 0.801618
3 threads: 0.661426
4 threads: 0.686879
8 threads: 0.670758
16 threads: 0.660346
Execution #8:
sequential: 1.084992
1 thread: 1.188528
2 threads: 0.734302
3 threads: 0.898492
4 threads: 0.708497
8 threads: 0.57322
16 threads: 0.562786
Execution #9:
sequential: 1.091029
1 thread: 1.193664
2 threads: 1.036727
3 threads: 0.861875
4 threads: 0.78906
8 threads: 0.736665
16 threads: 0.66056

Mean:
sequential: 0.9701098
1 thread: 1.0496157
2 threads: 0.6413002
3 threads: 0.6200687
4 threads: 0.5935748
8 threads: 0.5239579
16 threads: 0.5158447
