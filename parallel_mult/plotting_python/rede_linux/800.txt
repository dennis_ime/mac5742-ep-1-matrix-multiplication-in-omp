Execution #0:
sequential: 11.928217
1 thread: 11.234089
2 threads: 5.820493
3 threads: 6.1689
4 threads: 5.730735
8 threads: 5.756755
16 threads: 5.719452
Execution #1:
sequential: 10.737093
1 thread: 11.171188
2 threads: 5.778894
3 threads: 6.018347
4 threads: 5.732416
8 threads: 5.714263
16 threads: 5.711597
Execution #2:
sequential: 10.741798
1 thread: 11.150188
2 threads: 5.776326
3 threads: 5.793955
4 threads: 5.761332
8 threads: 5.72509
16 threads: 5.701546
Execution #3:
sequential: 10.718224
1 thread: 11.162187
2 threads: 5.768082
3 threads: 5.768728
4 threads: 5.759763
8 threads: 5.765058
16 threads: 5.865429
Execution #4:
sequential: 11.627157
1 thread: 11.41172
2 threads: 6.624503
3 threads: 6.931125
4 threads: 7.563342
8 threads: 6.407503
16 threads: 6.108003
Execution #5:
sequential: 10.904212
1 thread: 12.239973
2 threads: 7.496306
3 threads: 6.215998
4 threads: 5.739434
8 threads: 5.71232
16 threads: 5.602916
Execution #6:
sequential: 10.696968
1 thread: 11.160687
2 threads: 5.732174
3 threads: 5.703291
4 threads: 5.631924
8 threads: 5.65491
16 threads: 5.642809
Execution #7:
sequential: 10.701389
1 thread: 11.151636
2 threads: 6.396254
3 threads: 6.141428
4 threads: 6.032278
8 threads: 6.406221
16 threads: 5.930045
Execution #8:
sequential: 10.988358
1 thread: 12.097657
2 threads: 7.955175
3 threads: 6.704463
4 threads: 6.887355
8 threads: 5.735325
16 threads: 5.588221
Execution #9:
sequential: 10.67008
1 thread: 11.146845
2 threads: 5.724247
3 threads: 5.973159
4 threads: 5.808065
8 threads: 6.454665
16 threads: 6.521961

Mean:
sequential: 9.7785279
1 thread: 10.2692081
2 threads: 5.7251961
3 threads: 5.5250494
4 threads: 5.4915909
8 threads: 5.3575355
16 threads: 5.2672527
