import sys

import matplotlib.pyplot as plt
import numpy as np


def label_bars(plotter, rects):
	for rect in rects:
		height = rect.get_height()
		plotter.text(rect.get_x() + rect.get_width(), 1.02 * height, 
			'%fs'%float(height), ha='center', va='bottom', rotation=60)


matrix_size = 100

if len(sys.argv) >= 2:
	matrix_size = int(sys.argv[1])

commands = [SequentialMultCommand('../seq_mult.o', matrix_size),
			ParallelMultCommand('../par_mult.o', matrix_size, 1),
			ParallelMultCommand('../par_mult.o', matrix_size, 2),
			ParallelMultCommand('../par_mult.o', matrix_size, 3),
			ParallelMultCommand('../par_mult.o', matrix_size, 4),
			ParallelMultCommand('../par_mult.o', matrix_size, 8),
			ParallelMultCommand('../par_mult.o', matrix_size, 16)]

xlabels = []
yvalues = []

for command in commands:
	xlabel, yvalue = command.execute()
	xlabels.append(xlabel)
	yvalues.append(yvalue)

indexes = np.arange(len(xlabels))

bar_width = 0.6
labels_width = 0.85

fig = plt.figure()

plt.xticks(indexes + labels_width, xlabels)
rects = plt.bar(indexes + bar_width, yvalues, bar_width)
plt.ylabel('tempo decorrido (segundos)')
plt.xlim((indexes[0] + bar_width - 1.0, indexes[6] + 2*bar_width + 1.0))
plt.ylim((0, max(yvalues) * 1.3))


label_bars(plt, rects)
fig.autofmt_xdate()


filename = 'plot-matrix_size=' + str(matrix_size) + '.png'
plt.savefig(filename)
#plt.show()



print 'ploting has been saved as ' + filename