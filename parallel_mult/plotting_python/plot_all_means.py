#!/usr/bin/env python
# -*- coding: utf-8 -*-

from optparse import OptionParser

import numpy as np
import matplotlib.pyplot as plt

def extract_labels_values_means(text):
	labels, values = [], []
	mean_text = text[text.rfind('Mean:')+len("Mean:\n"): len(text)-1]
	lines = mean_text.split('\n')

	for line in lines:
		label, value = line.split(':')
		labels.append(label)
		values.append(float(value))

	return (labels, values)

parser = OptionParser()
parser.add_option('-s', '--source', dest='source', help='source directory', metavar='FILE', default='report.txt')
parser.add_option('-o', '--output', dest='output', help='output filename', metavar='FILE', default='graphic.png')

options, args = parser.parse_args()
s = options.source


filenames = [s + '/100.txt', s + '/200.txt', s +'/400.txt', s +'/800.txt', s + '/1200.txt']

matrix_mult = []
for filename in filenames:
	text = open(filename, 'r').read()
	result = extract_labels_values_means(text)
	matrix_mult.append(result)

labels_distance = 5
indexes = np.arange(len(matrix_mult[0][0]))

plt.xticks(indexes, matrix_mult[0][0])
plt.plot(matrix_mult[0][1])
plt.plot(matrix_mult[1][1])
plt.plot(matrix_mult[2][1])
plt.plot(matrix_mult[3][1])
plt.plot(matrix_mult[4][1])

plt.ylabel('Tempo de execução em segundos'.decode('utf8'))

plt.legend(['N=100', 'N=200', 'N=400', 'N=800', 'N=1200'])

plt.savefig(options.output)
#plt.show()




