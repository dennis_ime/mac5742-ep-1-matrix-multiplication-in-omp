from optparse import OptionParser
import numpy as np
import matplotlib.pyplot as plt

def extract_labels_values_means(text):
	labels, values = [], []
	mean_text = text[text.rfind('Mean:')+len("Mean:\n"): len(text)-1]
	lines = mean_text.split('\n')

	for line in lines:
		label, value = line.split(':')
		labels.append(label)
		values.append(float(value))

	return (labels, values)

def label_bars(plotter, rects):
	for rect in rects:
		height = rect.get_height()
		plotter.text(rect.get_x() + rect.get_width(), 1.02 * height, 
			'%fs'%float(height), ha='center', va='bottom', rotation=60)


parser = OptionParser()
parser.add_option('-s', '--source', dest='source', help='input filename', metavar='FILE', default='report.txt')
parser.add_option('-o', '--output', dest='output', help='output filename', metavar='FILE', default='graphic.png')
options, args = parser.parse_args()

source = open(options.source, 'r')

(labels, values) = extract_labels_values_means(source.read())


#============================ Plotting =======================================
indexes = np.arange(len(labels))

bar_width = 0.6
labels_width = 0.85

fig = plt.figure()

plt.xticks(indexes + labels_width, labels)
rects = plt.bar(indexes + bar_width, values, bar_width)
plt.ylabel('tempo decorrido (segundos)')
plt.xlim((indexes[0] + bar_width - 1.0, indexes[6] + 2*bar_width + 1.0))
plt.ylim((0, max(values) * 1.3))


label_bars(plt, rects)
fig.autofmt_xdate()

plt.savefig(options.output)
#plt.show()