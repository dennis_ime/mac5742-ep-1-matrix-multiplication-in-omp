import re
import os

class BarChartEntry:
	def __init__(self, label, value):
		self.label = label
		self.value = value

class ParallelMultCommand:
	def __init__(self, program_path, matrix_size, number_of_threads):
		self.program_path = program_path
		self.matrix_size = matrix_size
		self.number_of_threads = number_of_threads

	def execute(self):
		output = os.popen(self.program_path + ' ' + str(self.matrix_size) + ' ' + str(self.number_of_threads))
		lines = output.readlines()

		#lines[0] and lines[1] must be ignored because they are only debug strings
		time_elapsed = float(re.match('time elapsed: (.*) s', lines[2]).group(1))
		label_thread = 	' thread' if self.number_of_threads == 1 else ' threads'


		return (str(self.number_of_threads) + label_thread, time_elapsed)

class SequentialMultCommand:
	def __init__(self, program_path, matrix_size):
		self.program_path = program_path
		self.matrix_size = matrix_size

	def execute(self):
		output = os.popen(self.program_path + ' ' + str(self.matrix_size))
		lines = output.readlines()

		#lines[0] and lines[1] must be ignored because they are only debug strings
		time_elapsed = float(re.match('time elapsed: (.*) s', lines[2]).group(1))

		return ('sequential', time_elapsed)