#include <stdio.h>
#include <omp.h>

#define SIZE 16

int main() 
{
  int A[SIZE], B[SIZE], i;
  int aux_dot = 0;
  int dot = 0;
  
  for(i = 0; i < SIZE; i++)
  { 
       A[i] = i;
       B[i] = i; 
  }
  
  #pragma omp parallel firstprivate(aux_dot)
  {
    #pragma omp single
    printf("Inicio da regiao paralela, numero de threads = %d\n", omp_get_num_threads());
    
    #pragma omp for
    for(i = 0; i < SIZE; i++)
    { 
       aux_dot += A[i] * B[i]; 
       printf("Thread %d executa a iteracao %02d do loop: %d * %d = %d -> %d \n", omp_get_thread_num(), i, A[i], B[i], (A[i] * B[i]), aux_dot);
    }
    
    #pragma omp critical
    dot += aux_dot;
    
    #pragma omp barrier

    #pragma omp master
    printf("Produto final: %d.\n", dot);
  }
  
  return 0;
}